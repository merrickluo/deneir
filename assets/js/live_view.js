import { Socket } from "phoenix";
import LiveSocket from "phoenix_live_view";

const csrfToken = document
  .querySelector("meta[name='csrf-token']")
  .getAttribute("content");

// intesection observer is not working
const hooks = {
  articleExpand: {
    updated() {
      const container = document.querySelector("#article-container");
      container.scrollTop = this.el.offsetTop - 40;
    }
  },
  loadFeedIcon: {
    mounted() {
      const feedId = this.el.getAttribute("phx-value-id");
      const folderId = this.el.getAttribute("phx-value-folder-id");
      this.pushEventTo(`#feed-list-${folderId}`, "load-icon", feedId);
    }
  },
  loadMore: {
    mounted() {
      const target = this.el;
      const callback = (entries, observer) => {
        entries.forEach(entry => {
          if (entry.target == target && entry.isIntersecting) {
            this.pushEventTo("#article-list", "load-more");
          }
        });
      };
      this.observer = new IntersectionObserver(callback, {
        threshold: 0.3
      });
      this.observer.observe(target);
    }
  }
};

const liveSocket = new LiveSocket("/live", Socket, {
  params: { _csrf_token: csrfToken },
  hooks: hooks
});

liveSocket.connect();
liveSocket.enableDebug();
