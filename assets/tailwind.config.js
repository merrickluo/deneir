module.exports = {
  theme: {
    extend: {
      flex: {
        "l-24": "0 0 6rem"
      },
      width: {
        72: "18rem"
      },
      minWidth: {
        48: "12rem",
        72: "18rem"
      },
      padding: {
        "1/2": "0.125rem"
      },
      colors: {
        nord0: "#2e3440",
        nord1: "#3b4252",
        nord2: "#434c5e",
        nord3: "#4c566a",
        nord4: "#d8dee9",
        nord5: "#e5e9f0",
        nord6: "#eceff4",
        nord7: "#8fbcbb",
        nord8: "#88c0d0",
        nord9: "#81a1c1",
        nord10: "#5e81ac",
        nord11: "#bf616a",
        nord12: "#d08770",
        nord13: "#ebcb8b",
        nord14: "#a3be8c",
        nord15: "#b48ead"
        // background: "var(--color-background)",
        // "background-darker": "var(--color-background-)",
        // primary: "var(--color-primary)",
        // secondary: "var(--color-secondary)",
        // divider: "var(--color-divider)"
      },
      screens: {
        "dark-mode": { raw: "(prefers-color-scheme: dark)" }
      }
    }
  },
  variants: {},
  plugins: []
};
