defmodule DeneirWeb.FeedListComponent do
  use Phoenix.LiveComponent
  alias DeneirWeb.PageView
  alias Deneir.Store

  @impl true
  def mount(socket) do
    {:ok, socket, temporary_assigns: [feeds: []]}
  end

  @impl true
  def render(assigns) do
    PageView.render("feed-list.html", assigns)
  end

  @impl true
  def update(assigns, socket) do
    case assigns do
      %{feeds: feeds} ->
        {:ok, socket |> assign(feeds: feeds)}

      %{id: folder_id, store: store} ->
        feeds =
          Store.feeds(store)
          |> Enum.filter(fn feed ->
            feed.folder.id == folder_id
          end)

        {:ok, socket |> assign(folder_id: folder_id, store: store, feeds: feeds)}
    end
  end

  @impl true
  def handle_event("load-icon", feed_id, socket) do
    store = socket.assigns.store
    Store.load_feed_icon(store, String.to_integer(feed_id))
    Store.load_unread_count(store, feed_id: String.to_integer(feed_id))
    {:noreply, socket}
  end
end
