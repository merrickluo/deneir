defmodule DeneirWeb.SidebarLive do
  use Phoenix.LiveView

  alias DeneirWeb.{PageView, Navigator}
  alias Deneir.Store

  @impl true
  def mount(_params, %{"store" => store}, socket) do
    Phoenix.PubSub.subscribe(Deneir.PubSub, "feed_meta_updated")
    schedule_batch_update_feeds()

    {:ok, socket |> assign(:store, store) |> assign(pending_feeds: []),
     temporary_assigns: [folders: Store.folders(store)]}
  end

  @impl true
  def render(assigns) do
    PageView.render("sidebar.html", assigns)
  end

  @impl true
  def handle_event("select-folder", %{"id" => id}, socket) do
    store = socket.assigns.store
    folder = Store.folder(store, id: String.to_integer(id))
    Navigator.navigate(:patch, to: "/folders/#{folder.title}", replace: true)

    {:noreply, socket}
  end

  @impl true
  def handle_event("select-feed", %{"id" => id}, socket) do
    store = socket.assigns.store
    feed = Store.feed(store, id: String.to_integer(id))
    title = URI.encode(feed.title)
    Navigator.navigate(:patch, to: "/feeds/#{title}", replace: true)

    {:noreply, socket}
  end

  def handle_event("toggle-folder", %{"id" => id}, socket) do
    store = socket.assigns.store

    folder = Store.toggle_folder(store, String.to_integer(id))
    {:noreply, assign(socket, :folders, [folder])}
  end

  @impl true
  def handle_info({"feed_meta_updated", feed}, socket) do
    socket = assign(socket, :pending_feeds, [feed | socket.assigns.pending_feeds])
    # send_update(DeneirWeb.FeedListComponent, id: feed.folder.id, feed: feed)

    {:noreply, socket}
  end

  @impl true
  def handle_info(:batch_update_feed, socket) do
    IO.inspect(socket.assigns.pending_feeds)

    unless Enum.empty?(socket.assigns.pending_feeds) do
      socket.assigns.pending_feeds
      |> Enum.uniq_by(& &1.id)
      |> Enum.group_by(& &1.folder.id)
      |> Enum.each(fn {id, feeds} ->
        send_update(DeneirWeb.FeedListComponent, id: id, feeds: feeds)
      end)
    end

    schedule_batch_update_feeds()
    {:noreply, assign(socket, :pending_feeds, [])}
  end

  defp schedule_batch_update_feeds do
    Process.send_after(self(), :batch_update_feed, 3000)
  end
end
