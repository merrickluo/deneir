defmodule DeneirWeb.ArticleListLive do
  use Phoenix.LiveView

  alias DeneirWeb.PageView
  alias Deneir.Store

  @impl true
  def mount(_params, %{"filters" => filters, "store" => store}, socket) do
    socket =
      socket
      |> assign(offset: 0)
      |> assign(:open_article, nil)
      |> assign(:has_more, true)
      |> assign(:store, store)

    Store.set_filter(store, filters)

    {:ok, load_more(socket), temporary_assigns: [articles: []]}
  end

  @impl true
  def render(assigns) do
    PageView.render("article-list.html", assigns)
  end

  @impl true
  def handle_event("load-more", _, socket) do
    {:noreply, load_more(socket)}
  end

  def handle_event("toggle-article", %{"id" => id, "action" => action}, socket) do
    # %{store: store, open_article: op} = socket.assigns

    # socket =
    #   case {action, op, Store.article(store, id: String.to_integer(id))} do
    #     {"expand", nil, article} ->
    #       Store.mark_article(store, id: String.to_integer(id))

    #       socket
    #       |> assign(:articles, [Deneir.Rss.Article.expand(article)])
    #       |> assign(:open_article, article.id)

    #     {"expand", op, article} ->
    #       Store.mark_article(store, id: String.to_integer(id))
    #       old = Store.article(store, id: op)
    #       new = Deneir.Rss.Article.expand(article)
    #       socket |> assign(:articles, [old, new]) |> assign(:open_article, new.id)

    #     {"collapse", _op, article} ->
    #       socket |> assign(:articles, [article]) |> assign(:open_article, nil)
    #   end

    {:noreply, socket}
  end

  def load_more(socket) do
    %{store: store, offset: offset} = socket.assigns
    articles = Store.articles(store, offset)

    socket
    |> assign(:articles, articles)
    |> assign(:has_more, Enum.count(articles) != 0)
    |> assign(:offset, offset + Enum.count(articles))
  end
end
