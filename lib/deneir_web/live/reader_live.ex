defmodule DeneirWeb.ReaderLive do
  use Phoenix.LiveView
  use DeneirWeb.Navigator

  alias DeneirWeb.PageView
  alias Deneir.{Store, StoreCache}

  require Logger

  @impl true
  def render(assigns) do
    PageView.render("reader.html", assigns)
  end

  @impl true
  def mount(_params, session, socket) do
    register_navigation_handler()

    case session do
      %{"token" => _id} = args ->
        store = StoreCache.store_process(args)
        folders = Store.folders(store)

        {:ok, socket |> assign(filters: [], store: store), temporary_assigns: [folders: folders]}

      _ ->
        {:ok, push_redirect(socket, to: "/setup")}
    end
  end

  @impl true
  def handle_params(params, _url, socket) do
    unread = Map.get(params, "unread", false)
    store = socket.assigns.store

    socket =
      case params do
        %{"folder" => title} ->
          folder = Store.folder(store, title: title)
          assign(socket, filters: [folder_id: folder.id, unread: unread], title: title)

        %{"feed" => encoded_title} ->
          title = URI.decode(encoded_title)
          feed = Store.feed(store, title: title)
          assign(socket, filters: [feed_id: feed.id, unread: unread], title: title)

        _ ->
          assign(socket, filters: [unread: unread], title: "All")
      end

    {:noreply, socket}
  end
end
