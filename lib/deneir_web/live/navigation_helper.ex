defmodule DeneirWeb.Navigator do
  @topic "navigate"

  defmacro __using__(_opts) do
    quote do
      @impl true
      def handle_info({"navigate", :patch, args}, socket) do
        {:noreply, push_patch(socket, args)}
      end

      @impl true
      def handle_info({"navigate", :redirect, args}, socket) do
        {:noreply, push_redirect(socket, args)}
      end

      def register_navigation_handler do
        DeneirWeb.Navigator.register_navigation_handler()
      end
    end
  end

  def register_navigation_handler do
    Phoenix.PubSub.subscribe(Deneir.PubSub, "navigate")
  end

  def navigate(method, args) do
    Phoenix.PubSub.broadcast(Deneir.PubSub, @topic, {@topic, method, args})
  end
end
