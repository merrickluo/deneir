defmodule DeneirWeb.Router do
  use DeneirWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :live_view do
    plug :put_root_layout, {DeneirWeb.LayoutView, "app.html"}
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", DeneirWeb do
    pipe_through :browser
    pipe_through :live_view

    live "/", ReaderLive
    live "/folders/:folder", ReaderLive
    live "/feeds/:feed", ReaderLive
  end

  scope "/setup", DeneirWeb do
    pipe_through :browser
    get "/", SetupController, :index
    post "/", SetupController, :setup
  end

  scope "/icons", DeneirWeb do
    pipe_through :browser
    get "/", IconsController, :show
  end

  # Other scopes may use custom stacks.
  # scope "/api", DeneirWeb do
  #   pipe_through :api
  # end
end
