defmodule DeneirWeb.PageView do
  use DeneirWeb, :view

  @relative_types [
    {:year, "y"},
    {:week, "w"},
    {:day, "d"},
    {:hour, "h"},
    {:minute, "m"},
    {:second, "s"}
  ]
  def relative_time(time) do
    relative_time(Timex.now(), time, @relative_types)
  end

  def filter_id(filters) do
    filters
    |> Enum.reduce("", fn {k, v}, str -> "#{str}#{k}:#{v}" end)
  end

  defp relative_time(t1, t2, types) do
    [{type, format} | others] = types

    case Timex.diff(t1, t2, type) do
      0 -> relative_time(t1, t2, others)
      n -> "#{n}#{format}"
    end
  end
end
