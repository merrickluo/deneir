defmodule DeneirWeb.SetupController do
  use DeneirWeb, :controller

  def index(conn, _params) do
    case get_session(conn, :token) do
      nil ->
        render(conn, "index.html")

      _ ->
        redirect(conn, to: "/")
    end
  end

  def setup(conn, params) do
    %{"server" => server, "username" => username, "password" => password} = params

    miniflux = Deneir.Provider.Miniflux.new(server, username, password)

    if Deneir.Provider.Miniflux.check(miniflux) do
      token =
        Phoenix.Token.sign(
          DeneirWeb.Endpoint,
          "session_salt",
          "#{server}:#{username}:#{password}"
        )

      conn
      |> put_session(:token, token)
      |> put_session(:provider, "miniflux")
      |> put_session(:server, server)
      |> put_session(:username, username)
      |> put_session(:password, password)
      |> redirect(to: "/")
    else
      conn
      |> put_flash(:error, "Failed to setup, please check your config.")
      |> render("index.html")
    end
  end
end
