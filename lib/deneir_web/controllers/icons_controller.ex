defmodule DeneirWeb.IconsController do
  use DeneirWeb, :controller

  alias Deneir.StoreCache
  alias Deneir.Worker.FeedIconFetcher

  def show(conn, %{"url" => url}) do
    token = get_session(conn, :token)
    store = StoreCache.store_process(%{"token" => token})

    with server <- get_session(conn, :server),
         username <- get_session(conn, :username),
         password <- get_session(conn, :password),
         provider <- Deneir.Provider.Miniflux.new(server, username, password),
         fetcher <- :poolboy.checkout(:icon_fetcher) do
      case FeedIconFetcher.fetch(fetcher, store, provider, url) do
        nil ->
          redirect(conn, to: "/images/circle-regular.svg")

        {mime, icon} ->
          conn
          |> put_resp_content_type(mime)
          |> send_resp(:ok, icon)
      end
    end
  end
end
