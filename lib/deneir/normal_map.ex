defmodule Deneir.NormalMap do
  @doc "convert a list of map contains unique id to a map using id as key"
  @spec normalize(list(%{id: term()})) :: map()
  def normalize(list) do
    Enum.map(list, fn item ->
      {Map.get(item, :id), item}
    end)
    |> Enum.into(%{})
  end

  @spec flattern(map()) :: list(%{id: term()})
  def flattern(map) do
    Enum.map(map, fn {_id, item} ->
      item
    end)
    |> Enum.into([])
  end

  @spec get(map(), id :: term()) :: term()
  def get(map, id) do
    Map.get(map, id)
  end

  def find(map, predicate) do
    Enum.find_value(map, fn {_id, item} ->
      predicate.(item) && item
    end)
  end

  def get_and_update(map, id, fun) do
    Map.get_and_update(map, id, fn item ->
      new_item = fun.(item)
      {new_item, new_item}
    end)
  end
end
