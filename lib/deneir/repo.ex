defmodule Deneir.Repo do
  use Ecto.Repo,
    otp_app: :deneir,
    adapter: Ecto.Adapters.Postgres

  def upsert(changeset, target \\ :id) do
    insert(changeset, on_conflict: :replace_all, conflict_target: target)
  end
end
