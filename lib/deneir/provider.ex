defprotocol Deneir.Provider do
  def folders(provider)
  def feeds(provider)
  def articles(provider, filters)

  def unread_count(provider, feed_id)
  def icon(provider, feed_id)
  def mark_read(provider, article_id)
end
