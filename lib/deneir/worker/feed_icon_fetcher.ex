defmodule Deneir.Worker.FeedIconFetcher do
  use GenServer

  alias Deneir.IconCache

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  def init(_args) do
    {:ok, nil}
  end

  def fetch(pid, provider, feed) do
    GenServer.cast(pid, {:fetch, provider, feed})
  end

  def handle_cast({:fetch, provider, feed}, state) do
    icon =
      case IconCache.get(feed.feed_url) do
        :none ->
          nil

        nil ->
          Deneir.Provider.icon(provider, feed_id: feed.id)
      end

    Phoenix.PubSub.broadcast(
      Deneir.PubSub,
      "feed_icon_fetched",
      {"feed_icon_fetched", feed, icon}
    )

    {:noreply, state}
  end
end
