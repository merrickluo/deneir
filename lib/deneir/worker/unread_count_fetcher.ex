defmodule Deneir.Worker.UnreadCountFetcher do
  use GenServer

  alias Deneir.{Provider}

  @topic "unread_count_fetched"

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  @impl true
  def init(_args) do
    {:ok, nil}
  end

  def fetch(pid, provider, args) do
    GenServer.cast(pid, {:fetch, provider, args})
  end

  @impl true
  def handle_cast({:fetch, provider, feed_id: feed_id}, _nil) do
    unread_count = Provider.unread_count(provider, feed_id: feed_id)

    Phoenix.PubSub.broadcast(Deneir.PubSub, @topic, {
      @topic,
      :feed,
      feed_id,
      unread_count
    })

    {:noreply, nil}
  end

  @impl true
  def handle_cast({:fetch, provider, folder_id: folder_id}, _nil) do
    unread_count = Provider.unread_count(provider, folder_id: folder_id)

    Phoenix.PubSub.broadcast(Deneir.PubSub, @topic, {
      @topic,
      :folder,
      folder_id,
      unread_count
    })
  end
end
