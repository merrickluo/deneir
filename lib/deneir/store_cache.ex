defmodule Deneir.StoreCache do
  use GenServer

  alias Deneir.Store

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def init(_args) do
    {:ok, %{}}
  end

  def store_process(args) do
    GenServer.call(__MODULE__, {:store_process, args})
  end

  def handle_call({:store_process, %{"token" => id} = args}, _caller, cache) do
    case Map.get(cache, id) do
      nil ->
        %{"server" => server, "username" => username, "password" => password} = args

        {:ok, store} =
          Deneir.Provider.Miniflux.new(server, username, password)
          |> Store.start_link()

        {:reply, store, Map.put(cache, id, store)}

      store ->
        {:reply, store, cache}
    end
  end
end
