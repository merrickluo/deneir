defmodule Deneir.Store do
  use GenServer

  alias Deneir.Worker.{FeedIconFetcher, UnreadCountFetcher}
  alias Deneir.{Provider, NormalMap}

  defstruct provider: nil, folders: nil, feeds: nil, articles: [], filter: []

  alias __MODULE__

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  def folders(pid) do
    GenServer.call(pid, :folders)
  end

  def feeds(pid) do
    GenServer.call(pid, :feeds)
  end

  def articles(pid, offset \\ 0) do
    GenServer.call(pid, {:articles, offset})
  end

  def set_filter(pid, filter) do
    GenServer.cast(pid, {:set_filter, filter})
  end

  def toggle_folder(pid, folder_id) do
    GenServer.call(pid, {:toggle_folder, folder_id})
  end

  def load_feed_icon(pid, feed_id) do
    GenServer.cast(pid, {:load_feed_icon, feed_id})
  end

  def load_unread_count(pid, args) do
    GenServer.cast(pid, {:load_unread_count, args})
  end

  def feed(pid, query) do
    GenServer.call(pid, {:feed, query})
  end

  def folder(pid, query) do
    GenServer.call(pid, {:folder, query})
  end

  def article(pid, id: id) do
    articles(pid) |> Enum.find(&(&1.id == id))
  end

  def mark_article(pid, id: id) do
    GenServer.cast(pid, {:mark_article, id})
  end

  @impl true
  def init(provider) do
    Phoenix.PubSub.subscribe(Deneir.PubSub, "unread_count_fetched")
    Phoenix.PubSub.subscribe(Deneir.PubSub, "feed_icon_fetched")
    {:ok, %Store{provider: provider}}
  end

  @impl true
  def handle_cast({:set_filter, filter}, %Store{} = store) do
    {:noreply, %Store{store | filter: filter, articles: []}}
  end

  def handle_cast({:mark_article, id}, %Store{provider: provider, articles: articles} = store) do
    Deneir.Provider.mark_read(provider, article_id: id)

    articles =
      articles
      |> Enum.map(fn article ->
        case article do
          %{id: ^id} ->
            Map.put(article, :unread, false)

          _ ->
            article
        end
      end)

    {:noreply, %Store{store | articles: articles}}
  end

  @impl true
  def handle_cast({:load_feed_icon, feed_id}, %Store{feeds: feeds, provider: provider} = store) do
    Task.start(fn ->
      feed = NormalMap.get(feeds, feed_id)

      :poolboy.transaction(:feed_icon_fetcher, fn fetcher ->
        FeedIconFetcher.fetch(fetcher, provider, feed)
      end)
    end)

    {:noreply, store}
  end

  @impl true
  def handle_cast({:load_unread_count, feed_id: id}, %Store{provider: provider} = store) do
    Task.start(fn ->
      :poolboy.transaction(:unread_count_fetcher, fn fetcher ->
        UnreadCountFetcher.fetch(fetcher, provider, feed_id: id)
      end)
    end)

    {:noreply, store}
  end

  @impl true
  def handle_call({:toggle_folder, folder_id}, _caller, %Store{folders: folders} = store) do
    {updated_folder, folders} =
      NormalMap.get_and_update(folders, folder_id, fn folder ->
        Map.put(folder, :expanded, !folder.expanded)
      end)

    {:reply, updated_folder, %Store{store | folders: folders}}
  end

  @impl true
  def handle_call(:feeds, _caller, %Store{feeds: nil, provider: provider} = store) do
    feed_list = Provider.feeds(provider)
    {:reply, feed_list, %Store{store | feeds: feed_list |> NormalMap.normalize()}}
  end

  @impl true
  def handle_call(:feeds, _caller, %Store{feeds: feeds} = store) do
    {:reply, feeds |> NormalMap.flattern(), store}
  end

  @impl true
  def handle_call(:folders, _caller, %Store{folders: nil, provider: provider} = store) do
    folder_list = Provider.folders(provider)
    {:reply, folder_list, %Store{store | folders: folder_list |> NormalMap.normalize()}}
  end

  @impl true
  def handle_call(:folders, _caller, %Store{folders: folders} = store) do
    {:reply, folders |> NormalMap.flattern(), store}
  end

  @impl true
  def handle_call({:feed, id: id}, _caller, %Store{feeds: feeds} = store) do
    {:reply, NormalMap.get(feeds, id), store}
  end

  @impl true
  def handle_call({:feed, feed_url: url}, _caller, %Store{feeds: feeds} = store) do
    feed = NormalMap.find(feeds, fn feed -> feed.feed_url == url end)
    {:reply, feed, store}
  end

  @impl true
  def handle_call({:feed, title: title}, _caller, %Store{feeds: feeds} = store) do
    feed = NormalMap.find(feeds, fn feed -> feed.title == title end)
    {:reply, feed, store}
  end

  @impl true
  def handle_call({:folder, id: id}, _caller, %Store{folders: folders} = store) do
    {:reply, NormalMap.get(folders, id), store}
  end

  @impl true
  def handle_call({:folder, title: title}, _caller, %Store{folders: folders} = store) do
    folder = NormalMap.find(folders, fn folder -> folder.title == title end)
    {:reply, folder, store}
  end

  @impl true
  def handle_call({:articles, offset}, _caller, %Store{} = store) do
    articles = load_articles(store, offset)
    {:reply, articles |> Enum.drop(offset), %Store{store | articles: articles}}
  end

  @impl true
  def handle_info({"feed_icon_fetched", feed, icon}, %Store{feeds: feeds} = store) do
    {feed, feeds} =
      NormalMap.get_and_update(feeds, feed.id, fn feed ->
        Map.put(feed, :icon, icon)
      end)

    Phoenix.PubSub.broadcast(Deneir.PubSub, "feed_meta_updated", {"feed_meta_updated", feed})
    {:noreply, %Store{store | feeds: feeds}}
  end

  @impl true
  def handle_info({"unread_count_fetched", :feed, id, count}, %Store{feeds: feeds} = store) do
    {feed, feeds} =
      NormalMap.get_and_update(feeds, id, fn feed ->
        Map.put(feed, :unread_count, count)
      end)

    Phoenix.PubSub.broadcast(Deneir.PubSub, "feed_meta_updated", {"feed_meta_updated", feed})
    {:noreply, %{store | feeds: feeds}}
  end

  defp load_articles(%{articles: articles}, offset) when length(articles) > offset, do: articles

  defp load_articles(%{articles: articles, provider: provider, filter: filter}, offset) do
    new_articles = Deneir.Provider.articles(provider, Keyword.merge(filter, offset: offset))

    articles ++ new_articles
  end
end
