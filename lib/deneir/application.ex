defmodule Deneir.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Deneir.Repo,
      # Start the endpoint when the application starts
      DeneirWeb.Endpoint,
      Deneir.StoreCache,
      Deneir.IconCache,
      :poolboy.child_spec(:feed_icon_fetcher,
        name: {:local, :feed_icon_fetcher},
        worker_module: Deneir.Worker.FeedIconFetcher,
        size: 10,
        max_overflow: 5
      ),
      :poolboy.child_spec(:unread_count_fetcher,
        name: {:local, :unread_count_fetcher},
        worker_module: Deneir.Worker.UnreadCountFetcher,
        size: 10,
        max_overflow: 5
      )
      # Starts a worker by calling: Deneir.Worker.start_link(arg)
      # {Deneir.Worker, arg},
      # Deneir.IconCache,
      # Deneir.FeedIconFetcher
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Deneir.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    DeneirWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
