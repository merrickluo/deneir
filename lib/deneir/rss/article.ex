defmodule Deneir.Rss.Article do
  alias Deneir.Rss.Feed

  defstruct id: nil,
            title: "",
            url: "",
            feed: "",
            content: nil,
            unread: false,
            starred: false,
            expanded: false,
            author: nil,
            published_at: nil

  @type t :: %__MODULE__{
          id: integer,
          title: String.t(),
          url: String.t(),
          feed: Feed.t(),
          content: String.t(),
          unread: boolean,
          starred: boolean,
          author: String.t(),
          published_at: DateTime.t()
        }

  def from_miniflux(resp) do
    %Deneir.Rss.Article{
      id: resp.id,
      title: resp.title,
      url: resp.url,
      content: resp.content,
      unread: resp.status == "unread",
      starred: resp.starred,
      author: resp.author,
      published_at: Timex.parse!(resp.published_at, "{ISO:Extended}"),
      feed: Feed.from_miniflux(resp.feed)
    }
  end

  def expand(article) do
    article |> Map.put(:expanded, true) |> Map.put(:unread, false)
  end
end
