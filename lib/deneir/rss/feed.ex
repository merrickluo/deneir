defmodule Deneir.Rss.Feed do
  alias Deneir.Rss.Folder

  defstruct id: nil,
            title: "",
            site_url: "",
            feed_url: "",
            folder: nil,
            icon: nil,
            last_modified: nil,
            unread_count: nil

  @type t :: %__MODULE__{
          id: integer,
          title: String.t(),
          site_url: String.t(),
          feed_url: String.t(),
          folder: Folder.t(),
          last_modified: DateTime.t(),
          icon: binary,
          unread_count: integer | nil
        }

  def from_miniflux(resp) do
    last_modified =
      case Timex.parse(resp.last_modified_header, "{RFC1123}") do
        {:ok, time} -> time
        _ -> Timex.zero()
      end

    %Deneir.Rss.Feed{
      id: resp.id,
      title: resp.title,
      site_url: resp.site_url,
      feed_url: resp.feed_url,
      folder: Folder.from_miniflux(resp.category),
      last_modified: last_modified
    }
  end
end
