defmodule Deneir.Rss.Folder do
  defstruct id: nil, title: nil, feeds: [], expanded: false, unread_count: nil

  @type t :: %__MODULE__{
          id: integer,
          title: String.t(),
          expanded: boolean,
          feeds: list(Deneir.Rss.Feed.t()),
          unread_count: integer | nil
        }

  def from_miniflux(resp) do
    %Deneir.Rss.Folder{
      id: resp.id,
      title: resp.title
    }
  end

  def set(folder, key, value) do
    Map.put(folder, key, value)
  end
end
