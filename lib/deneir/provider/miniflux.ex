defmodule Deneir.Provider.Miniflux do
  defstruct client: nil

  alias Deneir.Provider.Miniflux
  alias Deneir.Rss.{Folder, Feed, Article}

  def new(server, user, pass) do
    middlewares = [
      {Tesla.Middleware.BaseUrl, server},
      {Tesla.Middleware.JSON, engine_opts: [keys: :atoms]},
      {Tesla.Middleware.BasicAuth, %{username: user, password: pass}}
    ]

    client = Tesla.client(middlewares)

    %Miniflux{client: client}
  end

  def folders(%Miniflux{client: client}) do
    {:ok, resp} = Tesla.get(client, "v1/categories")

    resp.body
    |> Enum.map(fn item ->
      Folder.from_miniflux(item)
    end)
  end

  def feeds(%Miniflux{client: client}) do
    {:ok, resp} = Tesla.get(client, "v1/feeds")

    resp.body
    |> Enum.map(fn item ->
      Deneir.Rss.Feed.from_miniflux(item)
    end)
    |> Enum.sort(fn l, r ->
      l.last_modified >= r.last_modified
    end)
  end

  def check(%Miniflux{client: client}) do
    {:ok, resp} = Tesla.get(client, "v1/me")
    resp.status == 200
  end

  # all unread articles
  def articles(%Miniflux{client: client}, unread: true, offset: offset) do
    fetch_entries(client, offset: offset, status: "unread")
  end

  # all articles
  def articles(%Miniflux{client: client}, unread: false, offset: offset) do
    fetch_entries(client, offset: offset)
  end

  # category unread articles
  def articles(%Miniflux{client: client}, folder_id: id, unread: true, offset: offset) do
    fetch_entries(client, category_id: id, offset: offset, status: "unread")
  end

  # category all articles
  def articles(%Miniflux{client: client}, folder_id: id, unread: false, offset: offset) do
    fetch_entries(client, category_id: id, offset: offset)
  end

  # feed unreader articles
  def articles(%Miniflux{client: client}, feed_id: id, unread: true, offset: offset) do
    fetch_feed_entries(client, id, offset: offset, status: "unread")
  end

  # feed all articles
  def articles(%Miniflux{client: client}, feed_id: id, unread: false, offset: offset) do
    fetch_feed_entries(client, id, offset: offset)
  end

  def icon(%Miniflux{client: client}, feed_id: id) do
    with {:ok, %{status: 200} = resp} <- Tesla.get(client, "v1/feeds/#{id}/icon") do
      resp.body.data
    else
      _ ->
        nil
    end
  end

  def unread_count(%Miniflux{client: client}, feed_id: id) do
    {:ok, resp} =
      Tesla.get(client, "v1/feeds/#{id}/entries", query: %{status: "unread", limit: 1})

    resp.body.total
  end

  def mark_read(%Miniflux{client: client}, article_id: id) do
    {:ok, resp} = Tesla.put(client, "v1/entries", %{entry_ids: [id], status: "read"})
    resp.status == 204
  end

  @default_query [direction: "desc", order: "published_at", limit: 50]
  defp fetch_entries(client, queries) do
    {:ok, resp} = Tesla.get(client, "v1/entries", query: Keyword.merge(@default_query, queries))
    resp.body.entries |> Enum.map(&Deneir.Rss.Article.from_miniflux/1)
  end

  defp fetch_feed_entries(client, feed_id, queries) do
    {:ok, resp} =
      Tesla.get(client, "v1/feeds/#{feed_id}/entries",
        query: Keyword.merge(@default_query, queries)
      )

    resp.body.entries |> Enum.map(&Deneir.Rss.Article.from_miniflux/1)
  end
end

defimpl Deneir.Provider, for: Deneir.Provider.Miniflux do
  alias Deneir.Provider.Miniflux

  def check(miniflux), do: Miniflux.check(miniflux)

  def folders(miniflux), do: Miniflux.folders(miniflux)
  def feeds(miniflux), do: Miniflux.feeds(miniflux)
  def articles(miniflux, filters), do: Miniflux.articles(miniflux, filters)

  def unread_count(miniflux, feed_id: id), do: Miniflux.unread_count(miniflux, feed_id: id)
  def icon(miniflux, feed_id: id), do: Miniflux.icon(miniflux, feed_id: id)
  def mark_read(miniflux, article_id: id), do: Miniflux.mark_read(miniflux, article_id: id)
end
