defmodule Deneir.IconCache do
  use GenServer

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def init(_args) do
    cache = :ets.new(:icon_cache, [:set, :protected])
    {:ok, cache}
  end

  def get(url) do
    GenServer.call(__MODULE__, {:get, url})
  end

  def set(url, icon) do
    GenServer.cast(__MODULE__, {:set, url, icon})
  end

  def handle_call({:get, feed}, _caller, cache) do
    case :ets.lookup(cache, feed) do
      [{^feed, icon}] -> {:reply, icon, cache}
      [] -> {:reply, nil, cache}
    end
  end

  def handle_cast({:set, feed, icon}, cache) do
    :ets.insert(cache, {feed, icon})
    {:noreply, cache}
  end
end
